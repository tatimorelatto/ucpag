using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankAccountNS;

namespace UcPagTests
{
    [TestClass]
    public class PagamentoTests
    {
        [TestMethod]
        public void deve_retornar_o_resultado()
        {
            // Arrange
            
                      
            double valorinicial = 100;
            int periodo = 5;
            double taxajuro = 0.01;
            double valorfinal = 105.10;

            // Act
            double resultadoAtual = valorfinal;


            // Assert
            Assert.AreEqual(valorfinal, resultadoAtual);


        }

    }
}
